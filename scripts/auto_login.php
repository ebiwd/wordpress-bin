<?php
/*
 * Script to auto login in wordpress for Local development
 *
 */
/** Make sure that the WordPress bootstrap has run before continuing. */
require(dirname(__FILE__) . '/wp-load.php');
$current_user = "";
$user_name ='admin';
$current_user = get_user_by('login', $user_name);
$user_id = isset($current_user) ? $current_user->ID : 1; // default 1 ID
wp_set_current_user($user_id, $user_name);
wp_set_auth_cookie($user_id);
do_action('wp_login', $user_name);
wp_redirect(home_url());
