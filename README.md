# Wordpress Development Setup

This folder contains scripts used to aid local & live wordpress development of various Wordpress sites & Group microsites using [Visual framework Wordpress Theme](https://git.embl.de/grp-stratcom/vf-wp).

## Installation of development scripts & instructions

Add as a submodule - `git submodule add --force https://gitlab.ebi.ac.uk/ebiwd/wordpress-bin.git bin`

To update a submodule - `git submodule update --remote --force bin`

You can edit `.env` file in which all variables configurations is defined like project URL, site title etc

### Usage of development scripts

- `bin/dev <options>` - spin quick/down/logs local development environment
- `bin/dev quick` to build default production wordpress site on local

### Typical workflow for creating a new site

- `rm -rf _*` - to clear existing database and files from folders starting with `_`
- `bin/dev up` - to spin up docker containers for local development
- `bin/dev quick` - to build default production wordpress site on local
- `bin/dev quick_blank` - to build blank wordpress website with Visual Framework plugin  & theme disabled mode
- `bin/dev quick_group` - to build wordpress website with basic Visual Framework default configuration - Plugin/themes enabled
- `bin/dev quick_group_bootstrap` - to build wordpress website setup with Visual Framework dummy microsite bootstrap version
- `bin/dev launch` - to launch browser
- `bin/dev down` - to spin down docker containers
- `bin/dev login` - to login in wordpress admin
- `bin/dev dump <sql|files|files-all|all> <dev|stage|prod> <backup>` - to get files/db dumps, 3rd parameter optional to get dump from backup


### Diagnostics

- `bin/dev logs` - tail logs from containers
- `bin/dev mailhog` - launch mailhog to view mail sent my containers
- `bin/dev pma` - launch phpMyAdmin to view database

### Pre-requisites (OSX)

You will need
- [Docker Community Edition](https://www.docker.com/community-edition#/download) insalled on your development machine
- SSH keys for drupal apache users in ~/.ssh, and ~/.ssh/config configured with (Required for pull DB/assets from VM server)
```
Host ves-ebi-?? ves-hx-?? ves-pg-?? ves-oy-?? wp-np?-?? wp-p?m-??
  IdentityFile ~/.ssh/%r
  StrictHostKeyChecking no
  BatchMode yes
```
